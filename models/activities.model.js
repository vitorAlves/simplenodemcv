module.exports = {


    listThreads(callback) {
        var sql = 'SELECT * from threads';
        global.connection.query(sql, function(error, rows, fields) {
            if (error) throw error;
            callback(rows);
        });
    },
    increment(data, callback) {
        var sql = "INSERT INTO hours (activity,session_duration, session_start, session_end, month, week, id_user) VALUES (?,?,?,?,?,?,?)";
        global.connection.query(
            sql, [data.id, data.duration, data.start, data.end, data.month, data.week, data.user],
            function(error, rows, fields) {
                if (error) throw error;
                callback(rows[0]);
            });





    },

    updateThread(data, callback) {
        var sql = "UPDATE activities SET thread=? WHERE id_activity=?";
        global.connection.query(
            sql, [data.thread, data.id],
            function(error, rows, fields) {
                if (error) throw error;
                callback(rows[0]);
            });


    },
    createToDo(data, callback) {
        var sql = "INSERT INTO activities (name_activity, thread,  id_client_fk, id_user_fk, description,type,subtype) VALUES (?,?,?,?,?,?,?)";
        global.connection.query(
            sql, [data.title, data.thread, data.client, data.user, data.description, data.type, data.sub],
            function(error, rows, fields) {
                if (error) throw error;
                callback(rows[0]);
            });





    },
    createThread(data, callback) {
        var sql = "INSERT INTO threads (name_thread, parent_thread, thread_type) VALUES (?,?, ?)";
        global.connection.query(
            sql, [data.name, data.parentThread, data.threadType],
            function(error, rows, fields) {
                if (error) throw error;
                callback(rows[0]);
            });





    },


    removeThread(id, callback) {
        var sql = "UPDATE threads set archived=1  WHERE id_thread=?";
        global.connection.query(
            sql, [id],
            function(error, rows, fields) {
                if (error) throw error;
                callback(rows[0]);
            });
    },






};
